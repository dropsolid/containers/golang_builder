FROM golang:1.18-alpine

RUN apk add --no-cache jq git curl

RUN curl -L -o packr_1.30.1_linux_amd64.tar.gz https://github.com/gobuffalo/packr/releases/download/v1.30.1/packr_1.30.1_linux_amd64.tar.gz && \
    tar -xzf packr_1.30.1_linux_amd64.tar.gz && \
   	rm packr_1.30.1_linux_amd64.tar.gz && \
    mv packr /usr/local/bin/packr
